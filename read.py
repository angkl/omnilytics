import re

f = open("output.txt", "r")
raw_str = f.read()
f.close()

def test_type(str):
    type_str = ''

    if str.isalpha():
        type_str = 'alphabetical strings'
    elif '.' in str:
        type_str = 'real numbers'
    elif str.isdigit():
        type_str = 'integer'
    elif bool(re.match('^(?=.*[a-z])(?=.*[0-9])', str)):
        type_str = 'alphanumeric'

    return type_str

raw_str = raw_str.replace('[','').replace(']','')
tokens = [token.strip() for token in raw_str.split(',')]

outputs = []
for token in tokens:
    token = token.strip()
    obj = {}
    obj['value'] = token
    obj['type'] = test_type(token)
    outputs.append(obj)

for output in outputs:
    print(f"{output['value']} - {output['type']}")

