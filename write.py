import random
import string
import sys
import re

class RandomGenerator():
    def generate_alpha_str(self):
        rand_str = ''
        str_len = random.randint(10,20)
        for _ in range(str_len):
            rand_str += random.choice(string.ascii_lowercase)

        return rand_str

    def generate_int(self):
        rand_int = random.randint(1,1000000000)
        return rand_int

    def generate_alphanum(self):
        rand_alphanum = ''
        str_len = random.randint(10, 20)
        for _ in range(str_len):
            rand_alphanum += random.choice(string.ascii_lowercase + string.digits)
        if not bool(re.match('^(?=.*[a-z])(?=.*[0-9])', rand_alphanum)): #check if contains at least 1 char and 1 digit
            rand_alphanum = self.generate_alphanum()
        start_space = self.__generate_random_space(1,10)
        end_space = self.__generate_random_space(1,10)
        rand_str = start_space+rand_alphanum+end_space

        return rand_str

    def generate_realnum(self):
        rand_realnum = random.uniform(0, 1000000)

        return rand_realnum

    def __generate_random_space(self, min_len, max_len):
        space_str=''
        for _ in range(min_len,max_len):
            space_str+=' '

        return space_str

random_str_list = []
random_generator = RandomGenerator()
while(sys.getsizeof(random_str_list)< 4e6):
    random_str_list.append(random_generator.generate_alpha_str())
    random_str_list.append(str(random_generator.generate_int()) )
    random_str_list.append(random_generator.generate_alphanum())
    random_str_list.append(str(random_generator.generate_realnum()) )

output_str = str(random_str_list).replace("'",'')

f = open("output.txt", "w")
f.write(output_str)

